package com.nextdots.elio.nextdots.user.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nextdots.elio.nextdots.MarvelApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.main.DI.ComicsComponent;
import com.nextdots.elio.nextdots.user.UserPresenter;
import com.nextdots.elio.nextdots.user.entities.User;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserDetailsFragment extends Fragment implements UserView {

    private UserPresenter presenter;
    @BindView(R.id.imgPhoto)
    ImageView imgPhoto;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtEmail)
    TextView txtEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, view);

        setupInjection();

        presenter.onCreate();
        presenter.loadUser();
        return view;
    }

    @Override
    public void onLoadError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadUser(User user) {
        txtEmail.setText(user.email);
        txtName.setText(user.fullName);
        if (!user.photoUrl.isEmpty()) {
            Glide.with(getActivity())
                    .load(user.photoUrl)
                    .placeholder(R.drawable.marvel_no_image)
                    .error(R.drawable.marvel_no_image)
                    .override(100, 100)
                    .fitCenter()
                    .into(imgPhoto);
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    public void setupInjection() {
        MarvelApp app = new MarvelApp();
        ComicsComponent component = app.getComicsComponent(getActivity(), this);
        presenter = component.getUserPresenter();
    }
}
