package com.nextdots.elio.nextdots.user;

import com.nextdots.elio.nextdots.user.entities.User;

public interface UserRepository {
    void loadUser();

    void addUser(User user);

    User getUser();
}
