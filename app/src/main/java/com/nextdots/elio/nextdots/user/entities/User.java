package com.nextdots.elio.nextdots.user.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {
    @PrimaryKey
    public String email;
    public String fullName;
    public String photoUrl;
}
