package com.nextdots.elio.nextdots.user;

public interface UserInteractor {
    void loadUser();
}