package com.nextdots.elio.nextdots.main;


import com.nextdots.elio.nextdots.main.UI.MainView;
import com.nextdots.elio.nextdots.main.events.MainEvent;

public interface MainPresenter {
    void onCreate();

    void onDestroy();

    void loadComics(boolean forceUpdate);

    void loadFavorites();

    void onEventMainThread(MainEvent event);

    MainView getView();
}
