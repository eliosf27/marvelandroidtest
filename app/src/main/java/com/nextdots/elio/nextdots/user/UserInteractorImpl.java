package com.nextdots.elio.nextdots.user;

import android.content.Context;

public class UserInteractorImpl implements UserInteractor {

    private UserRepository userRepository;
    private Context context;

    public UserInteractorImpl(Context context, UserRepository userRepository) {
        this.userRepository = userRepository;
        this.context = context;
    }

    @Override
    public void loadUser() {
        userRepository.loadUser();
    }
}