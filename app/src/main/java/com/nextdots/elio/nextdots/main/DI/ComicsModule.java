package com.nextdots.elio.nextdots.main.DI;


import android.content.Context;

import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.main.LocalRepository;
import com.nextdots.elio.nextdots.main.LocalRepositoryImpl;
import com.nextdots.elio.nextdots.main.MainInteractor;
import com.nextdots.elio.nextdots.main.MainInteractorImpl;
import com.nextdots.elio.nextdots.main.MainPresenter;
import com.nextdots.elio.nextdots.main.MainPresenterImpl;
import com.nextdots.elio.nextdots.main.NetworkRepository;
import com.nextdots.elio.nextdots.main.NetworkRepositoryImpl;
import com.nextdots.elio.nextdots.main.UI.MainView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

@Module
public class ComicsModule {
    private Context context;
    private MainView mainView;

    public ComicsModule(Context context, MainView mainView) {
        this.context = context;
        this.mainView = mainView;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return this.context;
    }

    @Provides
    @Singleton
    MainView providesMainView() {
        return this.mainView;
    }

    @Provides
    @Singleton
    MainPresenter providesMainPresenter(Context context, EventBus eventBus,
                                        MainView mainView, MainInteractor mainInteractor) {
        return new MainPresenterImpl(context, eventBus, mainView, mainInteractor);
    }

    @Provides
    @Singleton
    MainInteractor providesMainInteractor(Context context, LocalRepository localRepository, NetworkRepository networkRepository) {
        return new MainInteractorImpl(context, localRepository, networkRepository);
    }

    @Provides
    @Singleton
    LocalRepository providesLocalRepository(EventBus eventBus, Realm realm) {
        return new LocalRepositoryImpl(eventBus, realm);
    }

    @Provides
    @Singleton
    NetworkRepository providesMainRepository(EventBus eventBus, LocalRepository localRepository) {
        return new NetworkRepositoryImpl(eventBus, localRepository);
    }

    @Provides
    @Singleton
    Realm providesRealm() {
        return Realm.getDefaultInstance();
    }
}
