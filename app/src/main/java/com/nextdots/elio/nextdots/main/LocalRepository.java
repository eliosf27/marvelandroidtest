package com.nextdots.elio.nextdots.main;

import com.nextdots.elio.nextdots.main.entities.Comic;

import java.util.List;

import io.realm.RealmResults;

public interface LocalRepository {
    void loadComics(boolean isLostConnection);

    void loadComicById(String id);

    RealmResults<Comic> search(String text);

    void saveComics(List<Comic> comics);

    RealmResults<Comic> getComics();

    void deleteComics();

    void addFavorite(Comic comic);

    void removeFavorite(Comic comic);

    void loadFavorites();
}
