package com.nextdots.elio.nextdots.main.entities;

import io.realm.RealmObject;

public class ComicImage extends RealmObject {
    public String path;
    public String extension;
}
