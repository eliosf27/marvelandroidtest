package com.nextdots.elio.nextdots.lib.base;

public interface EventBus {
    void register(Object subscriber);

    void unregister(Object subscriber);

    void post(Object event);
}