package com.nextdots.elio.nextdots.main.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Item extends RealmObject {
    @PrimaryKey
    public String name;
    public String role;
    public String type;
}
