package com.nextdots.elio.nextdots.main.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Date extends RealmObject {
    @PrimaryKey
    public String type;
    public String date;
}
