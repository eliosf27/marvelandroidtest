package com.nextdots.elio.nextdots.domain;

public class Config {
     public static String API_URL = "https://gateway.marvel.com/v1/public/";
     public static String APP_TAG = "MARVEL_APP";
     public static String IS_FAVORITES = "IS_FAVORITES";
     public static String COMIC_ID = "COMIC_ID";
}
