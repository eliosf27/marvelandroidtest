package com.nextdots.elio.nextdots.main;

import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.main.entities.Comic;
import com.nextdots.elio.nextdots.main.events.MainEvent;

import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class LocalRepositoryImpl implements LocalRepository {
    private EventBus eventBus;
    private Realm realm;

    public LocalRepositoryImpl(EventBus eventBus, Realm realm) {
        this.eventBus = eventBus;
        this.realm = realm;
    }

    @Override
    public void loadComics(boolean isLostConnection) {
        RealmQuery<Comic> comics = realm.where(Comic.class);
        MainEvent event = new MainEvent();
        event.setType(MainEvent.onLoadDataLocalSuccess);
        event.setError(null);
        event.setComics(comics.findAllSorted("id"));
        event.setLostConnection(isLostConnection);
        eventBus.post(event);
    }

    @Override
    public void loadFavorites() {
        RealmQuery<Comic> comics = realm.where(Comic.class);
        MainEvent event = new MainEvent();
        event.setType(MainEvent.onLoadDataLocalFavoritesSuccess);
        event.setError(null);
        event.setComics(comics.equalTo("isFavorite", true).findAllSorted("id"));
        eventBus.post(event);
    }

    @Override
    public void saveComics(List<Comic> comics) {
        realm.executeTransaction(realm -> realm.copyToRealmOrUpdate(comics));
    }

    @Override
    public void deleteComics() {
        realm.executeTransaction(realm -> {
            RealmQuery<Comic> comics = realm.where(Comic.class);
            comics.equalTo("isFavorite", false).findAll().deleteAllFromRealm();
        });
    }

    @Override
    public RealmResults<Comic> getComics() {
        RealmQuery<Comic> categories = realm.where(Comic.class);
        return categories.findAllSorted("id");
    }

    @Override
    public void loadComicById(String id) {
        Comic comic = realm.where(Comic.class)
                .equalTo("id", id)
                .findFirst();
        MainEvent event = new MainEvent();
        event.setType(MainEvent.onLoadDetailsComic);
        event.setError(null);
        event.setComic(comic);
        eventBus.post(event);
    }

    @Override
    public RealmResults<Comic> search(String text) {
        return realm.where(Comic.class)
                .contains("title", text, Case.INSENSITIVE)
                .findAll();
    }

    @Override
    public void addFavorite(Comic comic) {
        realm.executeTransaction(realm -> {
            Comic copy = realm.copyToRealmOrUpdate(comic);
            copy.isFavorite = true;
        });
    }

    @Override
    public void removeFavorite(Comic comic) {
        realm.executeTransaction(realm -> {
            Comic copy = realm.copyToRealmOrUpdate(comic);
            copy.isFavorite = false;
        });
    }
}
