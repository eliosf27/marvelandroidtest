package com.nextdots.elio.nextdots.main.UI;

import com.nextdots.elio.nextdots.main.entities.Comic;

public interface ComicDetailsView {
    void onLoadComic(Comic comic);
}
