package com.nextdots.elio.nextdots.user;

import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.main.entities.Comic;
import com.nextdots.elio.nextdots.user.entities.User;
import com.nextdots.elio.nextdots.user.events.UserEvent;

import io.realm.Realm;
import io.realm.RealmQuery;

public class UserRepositoryImpl implements UserRepository {
    private EventBus eventBus;
    private Realm realm;

    public UserRepositoryImpl(EventBus eventBus, Realm realm) {
        this.eventBus = eventBus;
        this.realm = realm;
    }

    @Override
    public void addUser(User user) {
        realm.executeTransaction(realm -> {
            RealmQuery<User> users = realm.where(User.class);
            users.findAll().deleteAllFromRealm();
            RealmQuery<Comic> comics = realm.where(Comic.class);
            comics.findAll().deleteAllFromRealm();
            realm.copyToRealmOrUpdate(user);
        });
    }

    @Override
    public User getUser() {
        return realm.where(User.class)
                .findAll().first();
    }

    @Override
    public void loadUser() {
        User user = realm.where(User.class)
                .findFirst();
        UserEvent event = new UserEvent();
        event.setType(UserEvent.onLoadUserDetails);
        event.setError(null);
        event.setUser(user);
        eventBus.post(event);
    }
}
