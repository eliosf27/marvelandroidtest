package com.nextdots.elio.nextdots.user;


import com.nextdots.elio.nextdots.user.UI.UserView;
import com.nextdots.elio.nextdots.user.events.UserEvent;

public interface UserPresenter {
    void onCreate();

    void onDestroy();

    void loadUser();

    void onEventMainThread(UserEvent event);

    UserView getView();
}
