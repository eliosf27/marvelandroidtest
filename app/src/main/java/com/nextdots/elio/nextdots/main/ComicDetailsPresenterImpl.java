package com.nextdots.elio.nextdots.main;

import android.content.Context;

import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.main.UI.ComicDetailsView;
import com.nextdots.elio.nextdots.main.entities.Comic;
import com.nextdots.elio.nextdots.main.events.MainEvent;

import org.greenrobot.eventbus.Subscribe;

public class ComicDetailsPresenterImpl implements ComicDetailsPresenter {

    private EventBus eventBus;
    private ComicDetailsView comicDetailsView;
    private MainInteractor mainInteractor;
    private Context context;

    public ComicDetailsPresenterImpl(
            Context context, EventBus eventBus, ComicDetailsView comicDetailsView, MainInteractor mainInteractor) {
        this.context = context;
        this.eventBus = eventBus;
        this.comicDetailsView = comicDetailsView;
        this.mainInteractor = mainInteractor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        comicDetailsView = null;
        eventBus.unregister(this);
    }

    @Override
    public void loadComicDetails(String id) {
        mainInteractor.loadComicById(id);
    }


    @Override
    @Subscribe
    public void onEventMainThread(MainEvent event) {
        switch (event.getType()) {
            case MainEvent.onLoadDetailsComic:
                onLoadComicSuccess(event.getComic());
                break;
        }
    }


    private void onLoadComicSuccess(Comic comic) {
        if (comicDetailsView != null) {
            comicDetailsView.onLoadComic(comic);
        }
    }

    @Override
    public ComicDetailsView getView() {
        return comicDetailsView;
    }
}
