package com.nextdots.elio.nextdots;

import android.app.Application;
import android.content.Context;

import com.nextdots.elio.nextdots.lib.DI.LibsModule;
import com.nextdots.elio.nextdots.main.DI.ComicDetailsModule;
import com.nextdots.elio.nextdots.main.DI.ComicsComponent;
import com.nextdots.elio.nextdots.main.DI.ComicsModule;
import com.nextdots.elio.nextdots.main.DI.DaggerComicsComponent;
import com.nextdots.elio.nextdots.main.UI.ComicDetailsView;
import com.nextdots.elio.nextdots.main.UI.MainView;
import com.nextdots.elio.nextdots.user.DI.UserModule;
import com.nextdots.elio.nextdots.user.UI.UserView;

public class MarvelApp extends Application {
    public ComicsComponent getComicsComponent(Context context, MainView mainView) {
        return DaggerComicsComponent
                .builder()
                .libsModule(new LibsModule())
                .comicsModule(new ComicsModule(context, mainView))
                .comicDetailsModule(new ComicDetailsModule(context, null))
                .userModule(new UserModule(null))
                .build();
    }

    public ComicsComponent getComicsComponent(Context context, ComicDetailsView comicDetailsView) {
        return DaggerComicsComponent
                .builder()
                .libsModule(new LibsModule())
                .comicsModule(new ComicsModule(context, null))
                .comicDetailsModule(new ComicDetailsModule(context, comicDetailsView))
                .userModule(new UserModule(null))
                .build();
    }

    public ComicsComponent getComicsComponent(Context context, UserView userView) {
        return DaggerComicsComponent
                .builder()
                .libsModule(new LibsModule())
                .comicsModule(new ComicsModule(context, null))
                .comicDetailsModule(new ComicDetailsModule(context, null))
                .userModule(new UserModule(userView))
                .build();
    }
}
