package com.nextdots.elio.nextdots.user.DI;


import android.content.Context;

import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.user.UI.UserView;
import com.nextdots.elio.nextdots.user.UserInteractor;
import com.nextdots.elio.nextdots.user.UserInteractorImpl;
import com.nextdots.elio.nextdots.user.UserPresenter;
import com.nextdots.elio.nextdots.user.UserPresenterImpl;
import com.nextdots.elio.nextdots.user.UserRepository;
import com.nextdots.elio.nextdots.user.UserRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

@Module
public class UserModule {
    private UserView userView;

    public UserModule(UserView userView) {
        this.userView = userView;
    }

    @Provides
    @Singleton
    UserView providesUserView() {
        return this.userView;
    }

    @Provides
    @Singleton
    UserPresenter providesUserPresenter(Context context, EventBus eventBus, UserView userView, UserInteractor userInteractor) {
        return new UserPresenterImpl(context, eventBus, userView, userInteractor);
    }

    @Provides
    @Singleton
    UserInteractor providesUserInteractor(Context context, UserRepository userRepository) {
        return new UserInteractorImpl(context, userRepository);
    }

    @Provides
    @Singleton
    UserRepository providesUserRepository(EventBus eventBus, Realm realm) {
        return new UserRepositoryImpl(eventBus, realm);
    }
}
