package com.nextdots.elio.nextdots.main;

public interface MainInteractor {
    void loadComics();

    void loadComicsFromNetwork();

    void loadFavorites();

    void loadComicById(String id);
}