package com.nextdots.elio.nextdots.main;

import android.content.Context;

import com.nextdots.elio.nextdots.domain.NetworkUtils;

public class MainInteractorImpl implements MainInteractor {

    private LocalRepository localRepository;
    private NetworkRepository networkRepository;
    private Context context;

    public MainInteractorImpl(
            Context context, LocalRepository localRepository, NetworkRepository networkRepository) {
        this.localRepository = localRepository;
        this.networkRepository = networkRepository;
        this.context = context;
    }

    @Override
    public void loadComics() {
        if (localRepository.getComics().isEmpty()) {
            if (NetworkUtils.haveNetworkConnection(context)) {
                networkRepository.loadComics();
            } else {
                localRepository.loadComics(true);
            }
        } else {
            localRepository.loadComics(false);
        }
    }

    @Override
    public void loadComicsFromNetwork() {
        networkRepository.loadComics();
    }

    @Override
    public void loadFavorites() {
        localRepository.loadFavorites();
    }

    @Override
    public void loadComicById(String id) {
        localRepository.loadComicById(id);
    }
}