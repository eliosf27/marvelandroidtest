package com.nextdots.elio.nextdots.domain;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateRandomUtils {

    public static String getRandomDate(int initialYear, int lastYear) {
        Date date2 = randomDate(initialYear, lastYear);
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return df2.format(date2);
    }

    public static Date randomDate(int initialYear, int lastYear) {
        if (initialYear > lastYear) {
            int year = lastYear;
            lastYear = initialYear;
            initialYear = year;
        }

        Calendar cInitialYear = Calendar.getInstance();
        cInitialYear.set(Calendar.YEAR, initialYear);
        long offset = cInitialYear.getTimeInMillis();

        Calendar cLastYear = Calendar.getInstance();
        cLastYear.set(Calendar.YEAR, lastYear);
        long end = cLastYear.getTimeInMillis();

        long diff = end - offset + 1;
        Timestamp timestamp = new Timestamp(offset + (long) (Math.random() * diff));
        return new Date(timestamp.getTime());
    }
}
