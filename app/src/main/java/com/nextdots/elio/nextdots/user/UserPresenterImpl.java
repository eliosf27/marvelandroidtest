package com.nextdots.elio.nextdots.user;

import android.content.Context;

import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.user.UI.UserView;
import com.nextdots.elio.nextdots.user.entities.User;
import com.nextdots.elio.nextdots.user.events.UserEvent;

import org.greenrobot.eventbus.Subscribe;

public class UserPresenterImpl implements UserPresenter {

    private EventBus eventBus;
    private UserView userView;
    private UserInteractor userInteractor;
    private Context context;

    public UserPresenterImpl(
            Context context, EventBus eventBus, UserView userView, UserInteractor userInteractor) {
        this.context = context;
        this.eventBus = eventBus;
        this.userView = userView;
        this.userInteractor = userInteractor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        userView = null;
        eventBus.unregister(this);
    }

    @Override
    public void loadUser() {
        userInteractor.loadUser();
    }

    @Override
    @Subscribe
    public void onEventMainThread(UserEvent event) {
        switch (event.getType()) {
            case UserEvent.onLoadUserDetails:
                onLoadUserSuccess(event.getUser());
                break;
            case UserEvent.onLoadUserDetailsError:
                onDownloadError(event.getError());
                break;
        }
    }

    private void onDownloadError(String error) {
        if (userView != null) {
            userView.onLoadError(error);
        }
    }

    private void onLoadUserSuccess(User user) {
        if (userView != null) {
            userView.onLoadUser(user);
        }
    }

    @Override
    public UserView getView() {
        return this.userView;
    }
}
