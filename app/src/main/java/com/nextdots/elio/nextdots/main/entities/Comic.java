package com.nextdots.elio.nextdots.main.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Comic extends RealmObject {
    @PrimaryKey
    String id;
    public RealmList<ComicImage> images;
    public RealmList<Date> dates;
    public Creator creators;
    public Character characters;
    public Story stories;
    public String title;
    public String description;
    public int pageCount;
    public boolean isFavorite = false;
    public RealmList<Price> prices;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RealmList<ComicImage> getImages() {
        return images;
    }

    public void setImages(RealmList<ComicImage> images) {
        this.images = images;
    }

    public RealmList<Date> getDates() {
        return dates;
    }

    public void setDates(RealmList<Date> dates) {
        this.dates = dates;
    }

    public Creator getCreators() {
        return creators;
    }

    public void setCreators(Creator creators) {
        this.creators = creators;
    }

    public Character getCharacters() {
        return characters;
    }

    public void setCharacters(Character characters) {
        this.characters = characters;
    }

    public Story getStories() {
        return stories;
    }

    public void setStories(Story stories) {
        this.stories = stories;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public RealmList<Price> getPrices() {
        return prices;
    }

    public void setPrices(RealmList<Price> prices) {
        this.prices = prices;
    }
}
