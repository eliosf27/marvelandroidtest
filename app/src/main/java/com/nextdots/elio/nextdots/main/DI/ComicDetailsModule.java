package com.nextdots.elio.nextdots.main.DI;


import android.content.Context;

import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.main.ComicDetailsPresenter;
import com.nextdots.elio.nextdots.main.ComicDetailsPresenterImpl;
import com.nextdots.elio.nextdots.main.MainInteractor;
import com.nextdots.elio.nextdots.main.UI.ComicDetailsView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ComicDetailsModule {
    private Context context;
    private ComicDetailsView comicDetailsView;

    public ComicDetailsModule(Context context, ComicDetailsView comicDetailsView) {
        this.context = context;
        this.comicDetailsView = comicDetailsView;
    }

    @Provides
    @Singleton
    ComicDetailsView providesComicDetailsView() {
        return this.comicDetailsView;
    }

    @Provides
    @Singleton
    ComicDetailsPresenter providesComicDetailsPresenter(Context context, EventBus eventBus,
                                                        ComicDetailsView comicDetailsView,
                                                        MainInteractor mainInteractor) {
        return new ComicDetailsPresenterImpl(context, eventBus, comicDetailsView, mainInteractor);
    }
}
