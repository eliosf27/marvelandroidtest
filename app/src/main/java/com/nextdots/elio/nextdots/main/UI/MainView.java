package com.nextdots.elio.nextdots.main.UI;

import com.nextdots.elio.nextdots.main.entities.Comic;

import java.util.List;

public interface MainView {
    void showProgress();

    void hideProgress();

    void onDownloadError(String error);

    void onLostNetworkConnectionError(String error);

    void onLoadComics(List<Comic> comics);
}
