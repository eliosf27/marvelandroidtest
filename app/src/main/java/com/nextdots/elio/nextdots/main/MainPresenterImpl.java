package com.nextdots.elio.nextdots.main;

import android.content.Context;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.main.UI.MainView;
import com.nextdots.elio.nextdots.main.entities.Comic;
import com.nextdots.elio.nextdots.main.events.MainEvent;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class MainPresenterImpl implements MainPresenter {

    private EventBus eventBus;
    private MainView mainView;
    private MainInteractor mainInteractor;
    private Context context;

    public MainPresenterImpl(
            Context context, EventBus eventBus, MainView mainView, MainInteractor mainInteractor) {
        this.context = context;
        this.eventBus = eventBus;
        this.mainView = mainView;
        this.mainInteractor = mainInteractor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        mainView = null;
        eventBus.unregister(this);
    }

    @Override
    public void loadComics(boolean forceUpdate) {
        mainView.showProgress();
        if (forceUpdate) {
            mainInteractor.loadComicsFromNetwork();
        } else {
            mainInteractor.loadComics();
        }
    }

    @Override
    public void loadFavorites() {
        mainView.showProgress();
        mainInteractor.loadFavorites();
    }

    @Override
    @Subscribe
    public void onEventMainThread(MainEvent event) {
        switch (event.getType()) {
            case MainEvent.onLoadDataLocalSuccess:
                onLoadComicsSuccess(event.getComics());
                if (event.isLostConnection()) {
                    onLostNetworkConnectionError(
                            context.getString(R.string.message_lost_network_connection)
                    );
                }
                break;
            case MainEvent.onLoadDataLocalFavoritesSuccess:
                onLoadComicsSuccess(event.getComics());
                break;
            case MainEvent.onLoadDataNetworkSuccess:
                onLoadComicsSuccess(event.getComics());
                break;
            case MainEvent.onLoadComicsError:
                onDownloadError(event.getError());
                break;
        }
    }

    private void onDownloadError(String error) {
        if (mainView != null) {
            mainView.hideProgress();
            mainView.onDownloadError(error);
        }
    }

    private void onLostNetworkConnectionError(String error) {
        if (mainView != null) {
            mainView.hideProgress();
            mainView.onLostNetworkConnectionError(error);
        }
    }

    private void onLoadComicsSuccess(List<Comic> comics) {
        if (mainView != null) {
            mainView.hideProgress();
            mainView.onLoadComics(comics);
        }
    }

    @Override
    public MainView getView() {
        return mainView;
    }
}
