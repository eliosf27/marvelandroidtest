package com.nextdots.elio.nextdots.main.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Price extends RealmObject {
    @PrimaryKey
    public String type;
    public float price;
}
