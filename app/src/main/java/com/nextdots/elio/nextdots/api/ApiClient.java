package com.nextdots.elio.nextdots.api;

import com.nextdots.elio.nextdots.BuildConfig;
import com.nextdots.elio.nextdots.domain.DateRandomUtils;
import com.nextdots.elio.nextdots.domain.MD5Utils;
import com.nextdots.elio.nextdots.main.entities.Comic;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.nextdots.elio.nextdots.domain.Config.API_URL;

public class ApiClient {
    public static Call<Response> getComics() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Marvel marvel = retrofit.create(Marvel.class);

        String initialDate = DateRandomUtils.getRandomDate(2000, 2007);
        String LastDate = DateRandomUtils.getRandomDate(2007, 2016);
        String dateRange = initialDate + "," + LastDate;
        String[] params = getArgs();
        String publicApiKey = params[0];
        String hash = params[1];
        String ts = params[2];
        return marvel.comics(publicApiKey, hash, ts, 30, dateRange);
    }

    private static String[] getArgs() {
        String args[] = new String[3];
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String publicApiKey = BuildConfig.PUBLIC_API_KEY;
        String privateApiKey = BuildConfig.PRIVATE_API_KEY;
        String hash = MD5Utils.encrypt(ts + privateApiKey + publicApiKey);
        args[0] = publicApiKey;
        args[1] = hash;
        args[2] = ts;
        return args;
    }

    interface Marvel {
        @GET("comics")
        Call<Response> comics(
                @Query("apikey") String k,
                @Query("hash") String h,
                @Query("ts") String ts,
                @Query("limit") int limit,
                @Query("dateRange") String dateRange
        );
    }

    public static class Response {
        public Data data;
    }

    public static class Data {
        public List<Comic> results;
    }
}