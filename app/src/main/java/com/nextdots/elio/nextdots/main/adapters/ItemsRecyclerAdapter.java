package com.nextdots.elio.nextdots.main.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.main.entities.Item;

import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;


public class ItemsRecyclerAdapter extends
        RealmRecyclerViewAdapter<Item, ItemsRecyclerAdapter.ComicViewHolder> {

    public ItemsRecyclerAdapter(Context context, @Nullable List<Item> data, boolean autoUpdate) {
        super(context, (OrderedRealmCollection<Item>) data, autoUpdate);
    }

    @Override
    public ComicViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater.from(context).inflate(R.layout.cell_item, viewGroup, false);
        return new ComicViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(ComicViewHolder holder, int position) {
        Item item = this.getData().get(position);
        holder.txtName.setText(item.name);
        holder.txtRole.setText(item.role);
        holder.txtType.setText(item.type);
    }

    class ComicViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        TextView txtRole;
        TextView txtType;

        ComicViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtRole = (TextView) itemView.findViewById(R.id.txtRole);
            txtType = (TextView) itemView.findViewById(R.id.txtType);
        }
    }
}
