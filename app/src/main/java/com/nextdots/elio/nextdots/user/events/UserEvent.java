package com.nextdots.elio.nextdots.user.events;

import com.nextdots.elio.nextdots.user.entities.User;

public class UserEvent {
    public final static int onLoadUserDetails = 1;
    public final static int onLoadUserDetailsError = 2;

    private int type;
    private String error;
    private User user;

    public UserEvent() {
        this.error = null;
        this.type = 0;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}