package com.nextdots.elio.nextdots.main.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nextdots.elio.nextdots.MarvelApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.main.ComicDetailsPresenter;
import com.nextdots.elio.nextdots.main.DI.ComicsComponent;
import com.nextdots.elio.nextdots.main.adapters.ItemsRecyclerAdapter;
import com.nextdots.elio.nextdots.main.entities.Comic;
import com.nextdots.elio.nextdots.main.entities.ComicImage;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComicDetailsFragment extends Fragment implements ComicDetailsView {

    private ComicDetailsPresenter presenter;
    @BindView(R.id.imgPhoto)
    ImageView imgPhoto;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtDate)
    TextView txtDate;
    @BindView(R.id.txtPageNumber)
    TextView txtPageNumber;
    @BindView(R.id.recyclerViewCharacters)
    RecyclerView recyclerViewCharacters;
    @BindView(R.id.recyclerViewCreators)
    RecyclerView recyclerViewCreators;
    @BindView(R.id.recyclerViewStories)
    RecyclerView recyclerViewStories;
    private ItemsRecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comic_details, container, false);
        ButterKnife.bind(this, view);

        setupInjection();

        recyclerViewStories.setAdapter(adapter);
        recyclerViewStories.setHasFixedSize(true);
        recyclerViewStories.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerViewCreators.setAdapter(adapter);
        recyclerViewCreators.setHasFixedSize(true);
        recyclerViewCreators.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerViewCharacters.setAdapter(adapter);
        recyclerViewCharacters.setHasFixedSize(true);
        recyclerViewCharacters.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenter.onCreate();

        Bundle arguments = getArguments();
        String comicId = arguments.getString(Config.COMIC_ID);
        presenter.loadComicDetails(comicId);
        return view;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    public void setupInjection() {
        MarvelApp app = new MarvelApp();
        ComicsComponent component = app.getComicsComponent(getActivity(), this);
        presenter = component.getComicDetailsPresenter();
    }

    @Override
    public void onLoadComic(Comic comic) {
        txtTitle.setText(comic.title);
        txtDescription.setText(comic.description);
        String date = comic.dates.get(0).date;
        int index = date.indexOf("T");
        txtDate.setText(date.substring(0, index));
        txtPageNumber.setText(String.format("%d", comic.pageCount));
        txtPrice.setText(String.format("%s", comic.prices.get(0).price));
        if (!comic.images.isEmpty()) {
            ComicImage image = comic.images.get(0);
            Glide.with(getActivity())
                    .load(image.path + "/detail.jpg")
                    .override(500, 500)
                    .fitCenter()
                    .into(imgPhoto);
        }
        adapter = new ItemsRecyclerAdapter(getActivity(), comic.characters.items, true);
        recyclerViewCharacters.setAdapter(adapter);
        adapter = new ItemsRecyclerAdapter(getActivity(), comic.stories.items, true);
        recyclerViewStories.setAdapter(adapter);
        adapter = new ItemsRecyclerAdapter(getActivity(), comic.creators.items, true);
        recyclerViewCreators.setAdapter(adapter);
    }

}
