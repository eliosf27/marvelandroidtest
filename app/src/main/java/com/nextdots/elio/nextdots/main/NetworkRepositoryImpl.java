package com.nextdots.elio.nextdots.main;


import com.nextdots.elio.nextdots.api.ApiClient;
import com.nextdots.elio.nextdots.main.entities.Comic;
import com.nextdots.elio.nextdots.lib.base.EventBus;
import com.nextdots.elio.nextdots.main.events.MainEvent;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkRepositoryImpl implements NetworkRepository {
    private EventBus eventBus;
    private LocalRepository localRepository;

    public NetworkRepositoryImpl(EventBus eventBus, LocalRepository localRepository) {
        this.eventBus = eventBus;
        this.localRepository = localRepository;
    }

    @Override
    public void loadComics() {
        MainEvent event = new MainEvent();
        Call<ApiClient.Response> call = ApiClient.getComics();
        call.enqueue(new Callback<ApiClient.Response>() {
            @Override
            public void onResponse(Call<ApiClient.Response> call, Response<ApiClient.Response> response) {
                List<Comic> comics = response.body().data.results;
                localRepository.deleteComics();
                localRepository.saveComics(comics);
                event.setType(MainEvent.onLoadDataNetworkSuccess);
                event.setError(null);
                event.setComics(localRepository.getComics());
                eventBus.post(event);
            }

            @Override
            public void onFailure(Call<ApiClient.Response> call, Throwable t) {
                event.setType(MainEvent.onLoadComicsError);
                event.setError(t.getLocalizedMessage());
                event.setComics(null);
                eventBus.post(event);
            }
        });
    }
}
