package com.nextdots.elio.nextdots.main;


import com.nextdots.elio.nextdots.main.UI.ComicDetailsView;
import com.nextdots.elio.nextdots.main.events.MainEvent;

public interface ComicDetailsPresenter {
    void onCreate();

    void onDestroy();

    void loadComicDetails(String id);

    void onEventMainThread(MainEvent event);

    ComicDetailsView getView();
}
