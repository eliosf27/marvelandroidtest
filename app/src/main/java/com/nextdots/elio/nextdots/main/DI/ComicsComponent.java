package com.nextdots.elio.nextdots.main.DI;

import com.nextdots.elio.nextdots.lib.DI.LibsModule;
import com.nextdots.elio.nextdots.main.ComicDetailsPresenter;
import com.nextdots.elio.nextdots.main.LocalRepository;
import com.nextdots.elio.nextdots.main.MainPresenter;
import com.nextdots.elio.nextdots.user.DI.UserModule;
import com.nextdots.elio.nextdots.user.UserPresenter;
import com.nextdots.elio.nextdots.user.UserRepository;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ComicsModule.class, ComicDetailsModule.class, LibsModule.class, UserModule.class})
public interface ComicsComponent {
    MainPresenter getMainPresenter();

    UserPresenter getUserPresenter();

    ComicDetailsPresenter getComicDetailsPresenter();

    LocalRepository getLocalRepository();

    UserRepository getUserRepository();
}