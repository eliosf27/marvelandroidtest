package com.nextdots.elio.nextdots.main.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.nextdots.elio.nextdots.MarvelApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.main.DI.ComicsComponent;
import com.nextdots.elio.nextdots.main.LocalRepository;
import com.nextdots.elio.nextdots.main.MainPresenter;
import com.nextdots.elio.nextdots.main.adapters.ComicsRecyclerAdapter;
import com.nextdots.elio.nextdots.main.entities.Comic;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ComicsFragment extends Fragment implements MainView {

    @BindView(R.id.comicsRecyclerView)
    RecyclerView comicsRecyclerView;
    @BindView(R.id.btnLoad)
    Button btnLoad;
    private ProgressDialog mProgressDialog;
    private MainPresenter presenter;
    private ComicsRecyclerAdapter adapter;
    private LocalRepository localRepository;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Search comics");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(getActivity(), query, Toast.LENGTH_SHORT).show();
                searchView.setQuery("", false);
                searchView.setIconified(true);
                adapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comics, container, false);
        ButterKnife.bind(this, view);

        setupInjection();

        presenter.onCreate();

        Bundle arguments = getArguments();
        boolean isFavorites = arguments.getBoolean(Config.IS_FAVORITES);
        if (isFavorites) {
            presenter.loadFavorites();
            btnLoad.setVisibility(View.GONE);
        } else {
            presenter.loadComics(false);
            btnLoad.setVisibility(View.VISIBLE);
        }

        comicsRecyclerView.setAdapter(adapter);
        comicsRecyclerView.setHasFixedSize(true);
        comicsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        setHasOptionsMenu(true);
        return view;
    }

    public void setupInjection() {
        MarvelApp app = new MarvelApp();
        ComicsComponent component = app.getComicsComponent(getActivity(), this);
        presenter = component.getMainPresenter();
        localRepository = component.getLocalRepository();
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.messageLoadingComics));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    @OnClick(R.id.btnLoad)
    public void loadMore(View view) {
        presenter.loadComics(true);
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void onDownloadError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLostNetworkConnectionError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadComics(List<Comic> comics) {
        adapter = new ComicsRecyclerAdapter(getActivity(), comics, true, localRepository);
        comicsRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
        super.onDestroy();
    }
}
