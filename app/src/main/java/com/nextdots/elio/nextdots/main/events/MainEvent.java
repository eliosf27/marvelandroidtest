package com.nextdots.elio.nextdots.main.events;

import com.nextdots.elio.nextdots.main.entities.Comic;

import java.util.List;

public class MainEvent {
    public final static int onLoadDataLocalSuccess = 1;
    public final static int onLoadDataLocalFavoritesSuccess = 2;
    public final static int onLoadDataNetworkSuccess = 3;
    public final static int onLoadComicsError = 4;
    public final static int onLoadDetailsComic = 5;
    private int type;
    private String error;
    private List<Comic> comics;
    private Comic comic;
    private boolean isLostConnection;

    public MainEvent() {
        this.error = null;
        this.type = 0;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Comic getComic() {
        return comic;
    }

    public void setComic(Comic comic) {
        this.comic = comic;
    }

    public List<Comic> getComics() {
        return comics;
    }

    public void setComics(List<Comic> comics) {
        this.comics = comics;
    }

    public boolean isLostConnection() {
        return isLostConnection;
    }

    public void setLostConnection(boolean lostConnection) {
        isLostConnection = lostConnection;
    }
}