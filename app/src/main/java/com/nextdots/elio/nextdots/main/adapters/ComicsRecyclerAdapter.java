package com.nextdots.elio.nextdots.main.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.FragmentUtils;
import com.nextdots.elio.nextdots.main.LocalRepository;
import com.nextdots.elio.nextdots.main.UI.ComicDetailsFragment;
import com.nextdots.elio.nextdots.main.entities.Comic;
import com.nextdots.elio.nextdots.main.entities.ComicImage;

import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;


public class ComicsRecyclerAdapter extends
        RealmRecyclerViewAdapter<Comic, ComicsRecyclerAdapter.ComicViewHolder> {

    private LocalRepository localRepository;

    public ComicsRecyclerAdapter(Context context, @Nullable List<Comic> data,
                                 boolean autoUpdate, LocalRepository localRepository) {
        super(context, (OrderedRealmCollection<Comic>) data, autoUpdate);
        this.localRepository = localRepository;
    }

    @Override
    public ComicViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater.from(context).inflate(R.layout.cell_comic, viewGroup, false);
        return new ComicViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(ComicViewHolder holder, int position) {
        Comic comic = this.getData().get(position);
        float price = comic.prices.get(0).price;
        holder.txtTitle.setText(comic.title);
        if (price < 1) {
            holder.txtPrice.setText(R.string.message_sold_out_comic);
        } else {
            holder.txtPrice.setText("" + price + " $");
        }

        holder.btnLike.setOnClickListener(event -> {
            if (comic.isFavorite) {
                holder.btnLike.setImageResource(R.drawable.ic_favorite_border);
                localRepository.removeFavorite(comic);
            } else {
                holder.btnLike.setImageResource(R.drawable.ic_favorite);
                localRepository.addFavorite(comic);
            }
        });
        if (comic.isFavorite) {
            holder.btnLike.setImageResource(R.drawable.ic_favorite);
        } else {
            holder.btnLike.setImageResource(R.drawable.ic_favorite_border);
        }
        if (!comic.images.isEmpty()) {
            ComicImage image = comic.images.get(0);
            Glide.with(context)
                    .load(image.path + "/standard_fantastic.jpg")
                    .override(100, 100)
                    .fitCenter()
                    .into(holder.imgView);
        }
        holder.imgView.setOnClickListener(v -> {
            FragmentActivity activity = (FragmentActivity) context;
            Fragment fragment = new ComicDetailsFragment();
            Bundle arguments = new Bundle();
            arguments.putString(Config.COMIC_ID, comic.getId());
            FragmentUtils.setFragmentContent(activity, R.id.frag_content, fragment, arguments);
        });
        holder.txtTitle.setOnClickListener(v -> {
            FragmentActivity activity = (FragmentActivity) context;
            Fragment fragment = new ComicDetailsFragment();
            Bundle arguments = new Bundle();
            arguments.putString(Config.COMIC_ID, comic.getId());
            FragmentUtils.setFragmentContent(activity, R.id.frag_content, fragment, arguments);
        });
    }

    public void filter(String text) {
        OrderedRealmCollection<Comic> data = localRepository.search(text);
        updateData(data);
        notifyDataSetChanged();
    }

    class ComicViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtPrice;
        ImageView imgView;
        ImageView btnLike;
        CardView cardView;

        ComicViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            imgView = (ImageView) itemView.findViewById(R.id.imgView);
            btnLike = (ImageView) itemView.findViewById(R.id.btnLike);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
        }
    }
}
