package com.nextdots.elio.nextdots.user.UI;

import com.nextdots.elio.nextdots.user.entities.User;

public interface UserView {
    void onLoadError(String error);

    void onLoadUser(User user);
}
