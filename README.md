# Android App - Marvel API:
Project used to work with Marvel API 

## Instructions:

### Run the project:  
1. Clone the repo:  
   `git clone https://eliosf27@bitbucket.org/eliosf27/marvelandroidtest.git`  
2. Enter in the directory:  
   `cd marvelandroidtest/`    
3. Add a new file:  
   `nano app/gradle.properties`
4. Add the following key to the file
   (Note: These keys should never reach a git repository, in this case is a test):
   ```
   PUBLIC_API_KEY="2359abf629c50fbfd70cf59e778dabed"
   PRIVATE_API_KEY="aa879f60b89257f0d4711886a461c65f06ad349d"
   ```  
5. Open the project with Android Studio and build gradle:   
6. Thats it!!! Congratulations

**Technologies:**  
   Android SDK, MVP Clean Architecture, Retrofit, Dagger, EventBus, ButterKnife, YoYo Animations, Realm database, Material Design
